#!/bin/sh
ps aux | grep 'emacs *--daemon' > /dev/null || DISPLAY='' emacs --daemon -nw --no-splash

echo "$1"
if [ ! -z "$1" ]; then
    exec emacsclient -a "" "$@"
else
    # If any arguments is provided create a new frame without wait
    exec emacsclient -a "" -nc  "$@"
fi

